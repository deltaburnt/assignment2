/* EE422C Assignment 1 */
/* Student Name: Cassidy Burden (eid: cab5534),  Mason Bogowitz, Lab Section: Xavier (Th 9:30-11:00)*/

package assignment2;

import assignment2.GameInput.InputType;

public class A2Driver
{
	/**
	 * Determines if debug mode is on or off.
	 */
	public static final boolean DEBUG_MODE = true;

	public static void main(String[] args)
	{
		boolean playGame = true;

		GameInput input = new ConsoleGameInput();
		GameOutput output = new ConsoleGameOutput();

		output.outputRules();
		
		while(playGame)
		{
			Game mastermind = new Game(A2Driver.DEBUG_MODE);
			mastermind.runGame();
			
			output.outputPlayAgainPrompt();
			input.getInput();
			while(input.getInputType() != InputType.BOOLEAN)
			{
				output.outputError();
				output.outputPlayAgainPrompt();
				input.getInput();
			}
			
			playGame = input.getBooleanInput();
		}
	}
}
