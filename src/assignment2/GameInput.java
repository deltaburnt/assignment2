package assignment2;

public interface GameInput
{
	/**
	 * Possible valid input types
	 * @author Cassidy
	 *
	 */
	public static enum InputType
	{
		/**
		 * Input is a valid code
		 */
		CODE,
		
		/**
		 * Input for history list
		 */
		HISTORY,
		
		/**
		 * Input is boolean
		 */
		BOOLEAN,
		
		/**
		 * Input is invalid (none of the above)
		 */
		INVALID,
		
		/**
		 * Input for quitting (currently unused)
		 */
		QUIT
	}

	/**
	 * Blocking function to load input. Must be called before any other method in this class.
	 */
	public void getInput();
	
	/**
	 * Get type of current input
	 * @return the type of current stored input
	 */
	public InputType getInputType();
	
	/**
	 * Gets input code. Only to be called if getInputType returns InputType.CODE
	 * @return the currently stored input code
	 */
	public Code getInputCode();
	
	/**
	 * Gets boolean input. Only to be called if getInputType returns InputType.BOOLEAN
	 * @return the currently stored boolean input
	 */
	public boolean getBooleanInput();
}
