package assignment2;

import java.util.ArrayList;

/**
 * Represents the result of a guess code.
 * @author Cassidy
 *
 */

public class Result
{
	private int numBlack;
	private int numWhite;

	/**
	 * Constructs a result sequence based on user's inputed code and stored secret code.
	 * @param inputCode code input by user
	 * @param secretCode secret code user is trying to guess
	 */
	Result(Code inputCode, Code secretCode)
	{
		// Copy so original codes aren't changed
		ArrayList<Peg> inputPegs = (ArrayList<Peg>) inputCode.getPegList().clone();
		ArrayList<Peg> secretPegs = (ArrayList<Peg>) secretCode.getPegList().clone();
		
		// Find pegs that are in correct position for numBlack
		for(int i = 0; i < inputPegs.size(); i++)
		{
			if(inputPegs.get(i).equals(secretPegs.get(i)))
			{
				numBlack++;
				inputPegs.remove(i);
				secretPegs.remove(i);
				i--;
			}
		}
		
		// Find pegs that are right color but in wrong position for numWhite
		for(int i = 0; i < inputPegs.size(); i++)
		{
			if(secretPegs.contains(inputPegs.get(i)))
			{
				numWhite++;
				secretPegs.remove(inputPegs.get(i));
				inputPegs.remove(i);
				i--;
			}
		}
	}
	
	/**
	 * Get number of black pegs
	 * @return number of black pegs
	 */
	public int getBlackCount()
	{
		return numBlack;
	}
	
	/**
	 * Get number of white pegs
	 * @return number of white pegs
	 */
	public int getWhiteCount()
	{
		return numWhite;
	}
}
