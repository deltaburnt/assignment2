package assignment2;

import java.util.ArrayList;

/**
 * Console output implementation. For playing without a GUI.
 * @author Cassidy
 *
 */

public class ConsoleGameOutput implements GameOutput
{

	@Override
	public void outputRules()
	{
		System.out.println("Welcome to mastermind! You have " + Game.NUM_GUESSES +
		                   " tries to guess a " + Game.CODE_LENGTH + " length code.");
		System.out.println("You can choose from the following colors:");
		
		System.out.print(Color.values()[0]);
		for(int i = 1; i < Color.values().length; i++)
		{
			System.out.print(", " + Color.values()[i]);
		}
		
		System.out.println("\n");
	}

	@Override
	public void outputPrompt()
	{
		System.out.println("Please input your guess:");
	}

	@Override
	public void outputError()
	{
		System.out.println("Your input was invalid, please try again.");
		System.out.println();
	}
	

	@Override
	public void outputResult(Result result)
	{
		int numWhite = result.getWhiteCount();
		int numBlack = result.getBlackCount();
		System.out.println("Result: " + numBlack + " Black, " + numWhite + " White");
		System.out.println();
	}

	@Override
	public void outputLose(Code code)
	{
		System.out.println("Sorry, you lose! The secret code was: " + code + "\n\n");
	}
	
	@Override
	public void outputWin()
	{
		System.out.println("That's right! You win!\n\n");
	}
	

	@Override
	public void outputHistory(ArrayList<Code> guesses, ArrayList<Result> results)
	{
		if(guesses.size() == 0)
		{
			System.out.println("\nYou have made no guesses.\n");
			return;
		}
		
		System.out.println("\nYour guessing history is:");
		System.out.println("Guess\t\t# Black\t\t# White");
		
		for(int i = 0; i < guesses.size(); i++)
		{
			int numWhite = results.get(i).getWhiteCount();
			int numBlack = results.get(i).getBlackCount();

			System.out.print(guesses.get(i));
			System.out.println("\t\t" + numBlack + "\t\t" + numWhite);
		}
		
		System.out.println();
		
	}
	
	@Override
	public void outputDebugCode(Code secretCode)
	{
		System.out.println("DEBUG: The secret code is: " + secretCode.toString());
	}

	@Override
	public void clearOutput()
	{
		for(int i = 0; i < 50; i++)
		{
			System.out.println();
		}
	}

	@Override
	public void outputPlayAgainPrompt()
	{
		System.out.println("Would you like to play again (Y/N)?");
	}

}
