package assignment2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import assignment2.Color.InvalidColorException;

/**
 * Console input implementation. For playing without a GUI.
 * @author Cassidy
 *
 */

public class ConsoleGameInput implements GameInput
{
	private BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
	private Code inputCode;
	private InputType inputType;
	private String userInput;
	private Boolean userBoolInput;

	@Override
	public void getInput()
	{
		try
		{
			userInput = input.readLine();
		} catch (IOException e) {
		}
		
		if(userInput.equalsIgnoreCase("history"))
		{
			inputType = InputType.HISTORY;
			return;
		}
		
		if(userInput.matches("[ynYN]"))
		{
			inputType = InputType.BOOLEAN;
			userBoolInput = (userInput.equalsIgnoreCase("Y")) ? true : false;
			return;
		}
		
		if(userInput.length() != Game.CODE_LENGTH)
		{
			inputType = InputType.INVALID;
			return;
		}
		
		inputCode = new Code();
		
		try
		{
			for(int i = 0; i < Game.CODE_LENGTH; i++)
			{
				Color c = Color.getColor(userInput.charAt(i));
				Peg newPeg = new Peg(c);
				inputCode.addPeg(newPeg);
			}
		} catch (InvalidColorException e) {
			inputType = InputType.INVALID;
			return;
		}
		
		inputType = InputType.CODE;
	}

	@Override
	public InputType getInputType()
	{
		return inputType;
	}

	@Override
	public Code getInputCode()
	{
		return inputCode;
	}

	@Override
	public boolean getBooleanInput()
	{
		return userBoolInput;
	}

}
