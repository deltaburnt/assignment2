package assignment2;

import java.util.ArrayList;
import java.util.Random;

import assignment2.GameInput.InputType;

/**
 * Main Game class. Controls logic flow of the game.
 * @author Cassidy
 *
 */

public class Game
{
	/**
	 * Number of guesses the user gets before losing.
	 */
	public static final int NUM_GUESSES = 12;
	
	/**
	 * Number of pegs in the secret code.
	 */
	public static final int CODE_LENGTH = 4;

	private boolean debugMode;
	private Code secretCode = new Code();
	
	private ArrayList<Code> guessList = new ArrayList<Code>();
	private ArrayList<Result> resultList = new ArrayList<Result>();

	/**
	 * Makes a new mastermind game. New object made for each game played.
	 * @param debugMode when true outputs the secret code to the user.
	 */
	Game(boolean debugMode)
	{
		this.debugMode = debugMode;
	}
	
	/**
	 * Runs the game
	 */
	public void runGame()
	{
		generateCode();
		GameInput input = new ConsoleGameInput();
		GameOutput output = new ConsoleGameOutput();
		
		for(int i = 0; i < NUM_GUESSES; i++)
		{
			if(debugMode) { output.outputDebugCode(secretCode); }
			
			output.outputPrompt();
			input.getInput();
			
			if(input.getInputType() == InputType.INVALID)
			{
				output.outputError();
				continue;
			}
			
			if(input.getInputType() == InputType.HISTORY)
			{
				output.outputHistory(guessList, resultList);
				continue;
			}
			
			Code inputCode = input.getInputCode();
			if(inputCode.equals(secretCode))
			{
				output.outputWin();
				return;
			}
			
			Result result = new Result(inputCode, secretCode);
			guessList.add(inputCode);
			resultList.add(result);
			
			output.outputResult(result);
		}
		
		output.outputLose(secretCode);
	}
	
	private void generateCode()
	{
		Random rand = new Random();
		for(int i = 0; i < Game.CODE_LENGTH; i++)
		{
			Color randColor = Color.values()[rand.nextInt(Color.values().length)]; // Choose random color
			Peg newPeg = new Peg(randColor);
			secretCode.addPeg(newPeg);
		}
	}
}
